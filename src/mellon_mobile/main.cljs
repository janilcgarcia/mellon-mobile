(ns mellon-mobile.main
  (:require [react]
            [react-native :as rn]
            [reagent.core :as r]
            [mellon.generate :as gen]
            [mellon-mobile.crypto :as crypto]
            [clojure.core.async :as async :refer [go <!]]))

(defonce dict (js/require "../../data/default-dictionary.json"))

(defn- as-reagent
  [comp]
  (fn []
    (let [this (r/current-component)]
      (into [:> comp (r/props this)]
            (r/children this)))))

(def view (as-reagent rn/View))
(def text (as-reagent rn/Text))
(def button (as-reagent rn/Button))
(def status-bar (as-reagent rn/StatusBar))

(def current-passphrase (r/atom nil))

(defn generate-new-password
  [prbg _]
  (go
    (let [new-password (<! (gen/generate-passphrase prbg dict 4))]
      (swap! current-passphrase
             (fn [_]
               new-password))))
  true)

(defn grey
  [base]
  (str "#" base base base))

(defn navbar
  []
  [view {:style {:background-color (grey "37")
                 :margin-bottom 10
                 :height 56
                 :flex 0
                 :justify-content "center"
                 :border-bottom-color (grey "4A")
                 :border-bottom-width 2}}
   [text {:style {:font-size 28
                  :color "#EDEDED"
                  :font-weight :bold
                  :padding-left 20}}
    "Mellon"]])

(defn app
  [{:keys [prbg]}]
  [view {:style {:height "100%"
                 :background-color (grey "ED")}}
   [navbar]
   [view {:style {:background-color (grey "ED")
                  :flex 1
                  :justify-content "flex-start"
                  :padding-horizontal 20}} 
    [text {:style {:color "black"
                   :text-align "center"
                   :font-size 18
                   :margin-bottom 30}}
     "Welcome to Mellon! Here you can generate powerful passphrases"]

    [button {:title "Generate Passphrase"
             :on-press (partial generate-new-password prbg)}]

    [text {:selectable true
           :style {:font-size 22
                   :text-align "center"
                   :margin-top 10}}
     (or @current-passphrase "Here is where you passphrase will appear")]]])

(defn ^:export -main
  [& args]
  (let [prbg (crypto/system-prbg)]
    (r/as-element [app {:prbg prbg}])))
