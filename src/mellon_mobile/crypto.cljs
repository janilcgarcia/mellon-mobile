(ns mellon-mobile.crypto
  (:require ["react-native" :as rn]
            [mellon.random :as m.random]
            [clojure.core.async :as async :refer [close! <! >! go chan put!]]))

(defn- get-random
  [nbytes]
  (.getRandomBytes rn/NativeModules.SecureRandom nbytes))

(defn system-random
  [nbytes]
  (let [result-chan (chan)]
    (-> (get-random nbytes)
        (.then (fn [bytes]
                 (put! result-chan (vec bytes)
                       #(close! result-chan)))))
    result-chan))

(defn system-prbg
  []
  (m.random/->system-prbg system-random))
