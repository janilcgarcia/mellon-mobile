package com.mellonmobile

import com.facebook.react.bridge.*
import java.security.SecureRandom

class SecureRandomModule(context: ReactApplicationContext) : ReactContextBaseJavaModule(context) {
    val secureRandom = SecureRandom()
    override fun getName(): String = "SecureRandom"

    @ReactMethod
    fun getRandomBytes(length: Int, promise: Promise): Unit {
        val array = ByteArray(length)
        secureRandom.nextBytes(array)

        val jsArray = WritableNativeArray()

        for (byte in array) {
            jsArray.pushInt(byte.toInt())
        }

        promise.resolve(jsArray)
    }
}