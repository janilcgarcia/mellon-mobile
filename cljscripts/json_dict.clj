(ns json-dict
  (:require
   [clojure.string :as s]
   [clojure.data.json :as json]))

(defn convert
  [file-name]
  (let [file-contents (slurp file-name)
        words (s/split file-contents #"\s+")]
    (println
     (json/write-str words))))

(defn -main
  [& args]
  (if (< (count args) 1)
    (throw (ex-info "you must point to the input dict file" {}))
    (convert (first args))))
